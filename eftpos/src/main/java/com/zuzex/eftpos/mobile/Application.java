package com.zuzex.eftpos.mobile;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.logentries.android.AndroidLogger;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.CharSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;


/**
 * Created by dgureev on 23.12.14.
 */
public class Application extends android.app.Application {
    private static AndroidLogger mLogger;
    private static String unique_id = "";

    @Override
    public void onCreate() {
        createUniqueId();
        mLogger =  AndroidLogger.getLogger(getApplicationContext(), "31e7b29f-dd73-4ca4-8579-f25744443ae7", false);
        Logger l = LoggerFactory.getLogger(this.getClass());
        l.info("start logging");
        Log.w("UniqueID", getUniqueID());
    }

    public static void info(String message) {
        if(mLogger != null) {
            mLogger.info(unique_id+": "+message);
        }
    }

    private void createUniqueId() {
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        BluetoothAdapter m_BluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        String bluetooth_id = "";
        if(m_BluetoothAdapter != null) {
            bluetooth_id = m_BluetoothAdapter.getAddress();
        }
        unique_id = new String(Hex.encodeHex(DigestUtils.sha1(bluetooth_id+android_id)));
    }

    public static String getUniqueID() {
        if(unique_id != null) {
            return unique_id;
        }
        return "";
    }
}
