package com.zuzex.eftpos.mobile.Fragments;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;

import com.zuzex.eftpos.mobile.api.HttpApi;


/**
 * Created by dgureev on 17.12.14.
 */
public class FragmentMangerHelper implements PreloaderFragment.ProgressListener{
    private static final String CURRENT_FRAGMENT = "CURRENT_FRAGMENT_TAG";
    private static String currentFragmentTag;

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    private Activity activity;
    private int container;
    private PreloaderFragment mPreloaderFragment;

    public FragmentMangerHelper(Activity activity, int container, Bundle savedInstanceState) {
        this.activity = activity;
        this.container = container;

        if(savedInstanceState != null) {
            currentFragmentTag = savedInstanceState.getString(CURRENT_FRAGMENT);
        }

        if(mPreloaderFragment != null) {
            if(!tryLaunchLastFragment()) {
                loadFragmentByTag(PreloaderFragment.TAG, true);
                return;
            }
        }

        if(isValidLastFragment()) {
            loadFragmentByTag(currentFragmentTag, false);
        } else {
            if (HttpApi.getInstance().isAuthorized) {
                loadFragmentByTag(MainFragment.TAG, false);
            } else {
                loadFragmentByTag(PreloaderFragment.TAG, false);
            }
        }
    }
    public void loadFragmentByTag(final String tag, boolean appendToBackStack, String parameter) {
        final FragmentManager fm = activity.getFragmentManager();
        Fragment tmpFragment = fm.findFragmentByTag(tag);
        if(tag.equals(PreloaderFragment.TAG)) {
            mPreloaderFragment = new PreloaderFragment();
            mPreloaderFragment.setProgressListener(this);
            mPreloaderFragment.startLoading();
            fm.beginTransaction()
                    .replace(container, mPreloaderFragment, tag)
                    .commit();
            return;
        } else if(tag.equals(MainFragment.TAG)) {
            if(tmpFragment == null) {
                tmpFragment = new MainFragment();
            }
        } else if(tag.equals(LoginFragment.TAG)) {
            if (tmpFragment == null) {
                tmpFragment = new LoginFragment();
            }
        } else if(tag.equals(TestFragment.TAG)) {
            if (tmpFragment == null) {
                tmpFragment = new TestFragment();
            }

        }  else if(tag.equals(PreloaderFragment.TAG)) {
            if(tmpFragment == null) {
                tmpFragment = new PreloaderFragment();
            }
        }   else if(tag.equals(CheckFragment.TAG)) {
            if(tmpFragment == null) {
                tmpFragment = new CheckFragment();
                Bundle args = new Bundle();
                args.putString("parStr",parameter);
                tmpFragment.setArguments(args);

            }
        }
        else if(tag.equals(RegistrationDeviceFragment.TAG)) {
            if (tmpFragment == null) {
                tmpFragment = new RegistrationDeviceFragment();
            }
        }

        if(appendToBackStack) {
            fm.beginTransaction()
                    .replace(container, tmpFragment, tag)
                    .addToBackStack(tag)
                    .commit();
        } else {
            fm.beginTransaction()
                    .replace(container, tmpFragment, tag)
                    .commit();
        }
        setTitle(tag);
        currentFragmentTag = tag;
    }

    public void loadFragmentByTag(final String tag, boolean appendToBackStack) {
        final FragmentManager fm = activity.getFragmentManager();
        Fragment tmpFragment = fm.findFragmentByTag(tag);
        if(tag.equals(PreloaderFragment.TAG)) {
            mPreloaderFragment = new PreloaderFragment();
            mPreloaderFragment.setProgressListener(this);
            mPreloaderFragment.startLoading();
            fm.beginTransaction()
                    .replace(container, mPreloaderFragment, tag)
                    .commit();
            return;
        } else if(tag.equals(MainFragment.TAG)) {
            if(tmpFragment == null) {
                tmpFragment = new MainFragment();
            }
        } else if(tag.equals(LoginFragment.TAG)) {
            if (tmpFragment == null) {
                tmpFragment = new LoginFragment();
            }
        } else if(tag.equals(TestFragment.TAG)) {
            if (tmpFragment == null) {
                tmpFragment = new TestFragment();
            }

        }  else if(tag.equals(PreloaderFragment.TAG)) {
            if(tmpFragment == null) {
                tmpFragment = new PreloaderFragment();
            }
        }   else if(tag.equals(CheckFragment.TAG)) {
            if(tmpFragment == null) {
                tmpFragment = new CheckFragment();
            }
        }
        else if(tag.equals(RegistrationDeviceFragment.TAG)) {
            if (tmpFragment == null) {
                tmpFragment = new RegistrationDeviceFragment();
            }
        }

        if(appendToBackStack) {
            fm.beginTransaction()
                    .replace(container, tmpFragment, tag)
                    .addToBackStack(tag)
                    .commit();
        } else {
            fm.beginTransaction()
                    .replace(container, tmpFragment, tag)
                    .commit();
        }
        setTitle(tag);
        currentFragmentTag = tag;
    }

    boolean isValidLastFragment() {
        if (currentFragmentTag == null || currentFragmentTag.isEmpty()) {
            return false;
        }
        return true;
    }

    public Bundle onSaveInstanceState(Bundle outState) {
        outState.putString(CURRENT_FRAGMENT, currentFragmentTag);
        if(mPreloaderFragment != null) {
            mPreloaderFragment.onSaveInstanceState(outState);
        }
        return outState;
    }

    public boolean tryLaunchLastFragment() {
        if(isValidLastFragment()) {
            loadFragmentByTag(currentFragmentTag, true);
            return true;
        }
        return false;
    }

    @Override
    public void onCompletion(Boolean result) {
        if(result) {
            mPreloaderFragment = null;
            loadFragmentByTag(MainFragment.TAG, false);
        } else {
            loadFragmentByTag(LoginFragment.TAG, false);
        }
    }

    @Override
    public void onProgressUpdate(int value) {
        mPreloaderFragment.setProgress(value);
    }

    private boolean checkCompletionStatus() {
        if (mPreloaderFragment != null && mPreloaderFragment.hasResult()) {
            onCompletion(mPreloaderFragment.getResult());
            mPreloaderFragment.setProgressListener(null);
            mPreloaderFragment = null;
            return true;
        }
        return false;
    }

    public void onStart() {
        if (mPreloaderFragment != null) {
            checkCompletionStatus();
        }
    }

    public void onStop() {
        if (mPreloaderFragment != null) {
            mPreloaderFragment.removeProgressListener();
        }
    }

    private void setTitle(String text) {
        if(text != null) {
            ActionBar bar = activity.getActionBar();
            if(bar != null) {
                bar.setTitle(text);
            }
        }
    }

}
