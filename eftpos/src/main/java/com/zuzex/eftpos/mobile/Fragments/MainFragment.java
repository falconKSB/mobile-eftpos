package com.zuzex.eftpos.mobile.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.zuzex.eftpos.mobile.R;

import com.zuzex.eftpos.mobile.FullScreenActivity;


public class MainFragment extends StyledFragment implements View.OnClickListener {
    public static final String TAG = "MainFragment";
    public MainFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        updateBackground(rootView);

        Button posButton = (Button) rootView.findViewById(R.id.main_start_pos);
        posButton.setOnClickListener(this);
        Button registerDeviceButton = (Button) rootView.findViewById(R.id.main_registration_device_button);
        registerDeviceButton.setOnClickListener(this);
        Button fullScreenActivityStart = (Button) rootView.findViewById(R.id.main_fullscreen);
        fullScreenActivityStart.setOnClickListener(this);
        ImageView logoView = (ImageView) rootView.findViewById(R.id.logo);
        updateLogo(logoView);
        return rootView;
    }
    protected void updateLogo(ImageView imageView) {
        Context ct = getActivity();
        SharedPreferences preferences = ct.getSharedPreferences(STYLE_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String logoUrl = preferences.getString(STYLE_LOGO, "");
        Picasso.with(getActivity()).load(logoUrl).into(imageView);
        //Picasso.with(getActivity()).load("http://sefiani.com.au/wp-content/uploads/2011/02/EFTPOS_RGB_Large.gif").into(imageView);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_start_pos:
                getmFragmentMangerHelper().loadFragmentByTag(TestFragment.TAG, true);
                break;
            case R.id.main_registration_device_button:
                getmFragmentMangerHelper().loadFragmentByTag(RegistrationDeviceFragment.TAG, true);
                break;
            case R.id.main_fullscreen:
                Intent intent = new Intent(getActivity(), FullScreenActivity.class);
                intent.putExtra(FullScreenActivity.LOGO, getLogoUrl());
                intent.putExtra(FullScreenActivity.BACKGROUND, getColor());
                startActivity(intent);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case Activity.RESULT_OK:
                break;
            }
    }
}
