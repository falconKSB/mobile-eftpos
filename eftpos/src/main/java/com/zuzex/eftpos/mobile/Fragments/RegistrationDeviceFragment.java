package com.zuzex.eftpos.mobile.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.badoo.mobile.util.WeakHandler;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.zuzex.eftpos.mobile.Application;
import com.zuzex.eftpos.mobile.R;
import com.zuzex.eftpos.mobile.Utils.UiHelper;
import com.zuzex.eftpos.mobile.api.HttpApi;

import java.io.IOException;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationDeviceFragment extends StyledFragment {
    public static final String TAG = "RegistrationDeviceFragment";
    private EditText editTextUniqueId;
    private Spinner spinnerDeviceType;
    private EditText editTextDeviceName;
    private Button buttorRegistrationDevice;
    private String[] deviceTypeList;
    private ArrayAdapter<String> deviceTypeAdapter;
    Callback deviceRegistrationCallback;
    WeakHandler mHandler;
    private Button back;


    public RegistrationDeviceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mHandler = new WeakHandler();
//        (ble|wifi|pos|app)
        deviceTypeList = new String[] {"ble", "wifi", "pos", "app"};
        deviceRegistrationCallback = new Callback() {
            @Override
            public void onFailure(Request request, final IOException e) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        UiHelper.showToast(getActivity(), e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(final Response response) throws IOException {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        UiHelper.showToast(getActivity(), response.message());
                    }
                });
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_registration_device, container, false);
        editTextUniqueId = (EditText) rootView.findViewById(R.id.device_unique_id);
        editTextUniqueId.setText(Application.getUniqueID());
        back = (Button) rootView.findViewById(R.id.backRegistrationButton);
        //spinnerDeviceType = (Spinner) rootView.findViewById(R.id.device_type_spinner);
        //deviceTypeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, deviceTypeList);
        //deviceTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //spinnerDeviceType.setAdapter(deviceTypeAdapter);
        editTextDeviceName = (EditText) rootView.findViewById(R.id.device_name);

        buttorRegistrationDevice = (Button) rootView.findViewById(R.id.device_register_button);

        buttorRegistrationDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fieladsIsValid()) {
                    String deviceId = editTextUniqueId.getText().toString();
                    //String deviceType = spinnerDeviceType.getSelectedItem().toString();
                    String deviceType = "pos";
                    String name = editTextDeviceName.getText().toString();
                    HttpApi.getInstance().registerDevice(deviceId, deviceType, name, deviceRegistrationCallback);
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getmFragmentMangerHelper().loadFragmentByTag("MainFragment",true);
            }
        });

        updateBackground(rootView);
        return rootView;
    }

    private boolean fieladsIsValid() {
        if(editTextUniqueId.getText().toString().isEmpty()) {
            UiHelper.showToast(getActivity(), "error: id is empty");
            return false;
        }
        if(editTextDeviceName.getText().toString().isEmpty()) {
            UiHelper.showToast(getActivity(), "error: device name is empty");
            return false;
        }
        return true;
    }


}
