package com.zuzex.eftpos.mobile.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.zuzex.eftpos.mobile.MainActivity;
import com.zuzex.eftpos.mobile.R;
import com.zuzex.eftpos.mobile.Models.BaseModel;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dgureev on 17.12.14.
 */
public abstract class StyledFragment extends Fragment {
    public static int getColor() {
        return color;
    }

    public static String getLogoUrl() {
        return logoUrl;
    }

    private static int color = 0;
    private static String logoUrl = "";

    protected static final String STYLE_PREFERENCES_NAME = "style_preferences";

    protected static final String STYLE_LOGO = "style_logo";
    protected static final String STYLE_BACKGROUND_COLOR = "background_color";

    public FragmentMangerHelper getmFragmentMangerHelper() {
        return mFragmentMangerHelper;
    }

    private FragmentMangerHelper mFragmentMangerHelper;

    protected void updateBackground(View rootView) {
        rootView.setBackgroundColor(color);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(mFragmentMangerHelper == null) {
            mFragmentMangerHelper = ((MainActivity) activity).getmFragmentMangerHelper();
        }
        if(logoUrl.isEmpty()) {
            Context ct = getActivity();
            if(ct != null) {
                SharedPreferences preferences = ct.getSharedPreferences(STYLE_PREFERENCES_NAME, Context.MODE_PRIVATE);
                logoUrl = preferences.getString(STYLE_LOGO, "");
                color = preferences.getInt(STYLE_BACKGROUND_COLOR, 0);
            }
        }
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentMangerHelper = ((MainActivity) getActivity()).getmFragmentMangerHelper();
    }

    private void saveSettings() {
        Context ct = getActivity();
        if(ct != null) {
            SharedPreferences preferences = ct.getSharedPreferences(STYLE_PREFERENCES_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor ed = preferences.edit();
            ed.putString(STYLE_LOGO, logoUrl);
            ed.putInt(STYLE_BACKGROUND_COLOR, color);
            ed.commit();
        }
    }

    protected void updateLogo(ImageView imageView) {
        if(logoUrl != null && !logoUrl.isEmpty()) {
            Picasso.with(getActivity()).load(logoUrl).into(imageView);
        } else {
            Picasso.with(getActivity()).load(R.drawable.example_logo).into(imageView);
        }
    }

    public void loadStyleFromBaseModel(BaseModel baseModel) throws JSONException {
        JSONObject jsonObject = baseModel.getData();
        logoUrl = jsonObject.optString("logo");
        color = Color.parseColor(jsonObject.getString("background"));
        saveSettings();
    }

    private void loadDefaultStyle() {
        color = Color.parseColor("#FFFFFF");
        logoUrl = "http://sefiani.com.au/wp-content/uploads/2011/02/EFTPOS_RGB_Large.gif";
    }
}
