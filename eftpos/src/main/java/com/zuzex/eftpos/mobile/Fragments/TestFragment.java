package com.zuzex.eftpos.mobile.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.questps.cloudeftpossdk.CloudEftposSDK;
import com.questps.cloudeftpossdk.callbacks.OnAuthCompleted;
import com.questps.cloudeftpossdk.callbacks.OnPairingCompleted;
import com.questps.cloudeftpossdk.callbacks.OnStatus;
import com.questps.cloudeftpossdk.callbacks.OnTransactionCompleted;
import com.questps.cloudeftpossdk.callbacks.OnUpdateCompleted;
import com.questps.cloudeftpossdk.messages.AuthCompleted;
import com.questps.cloudeftpossdk.messages.ErrorResult;
import com.questps.cloudeftpossdk.messages.PairingCompleted;
import com.questps.cloudeftpossdk.messages.StatusChange;
import com.questps.cloudeftpossdk.messages.TransactionCompleted;
import com.questps.cloudeftpossdk.messages.TransactionRequest;
import com.questps.cloudeftpossdk.messages.UpdateCompleted;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import com.zuzex.eftpos.mobile.Application;
import com.zuzex.eftpos.mobile.R;
import com.zuzex.eftpos.mobile.Utils.UiHelper;
import com.zuzex.eftpos.mobile.Models.DeviceInfo;
import com.zuzex.eftpos.mobile.Models.UserInfo;
import com.zuzex.eftpos.mobile.api.HttpApi;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.questps.cloudeftpossdk.CloudEftposSDK.TransactionType.Purchase;
import static com.questps.cloudeftpossdk.CloudEftposSDK.TransactionType.Refund;

public class TestFragment extends StyledFragment implements View.OnClickListener {
    public static final String TAG = "TestFragment";
    private static CloudEftposSDK cloudEftposSDK;
    private UserInfo userInfo;
    private DeviceInfo deviceInfo;
    private EditText editText;
    private Button button;
    private Button auth;
    private Button back;
    private int transactionReference = 1;
    SharedPreferences userDetails;

    public TestFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userDetails = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userInfo = new UserInfo(userDetails.getString("userName",""),userDetails.getString("password",""));
        deviceInfo = new DeviceInfo();
        transactionReference = userDetails.getInt("PosReference",0);
        cloudEftposSDK = new CloudEftposSDK(getActivity(),false);
        cloudEftposSDK.RegisterForStatusChanges(new OnStatus() {
            @Override
            public void OnStatusChanged(StatusChange statusChange) {
                String textToShow = "";
                if(statusChange.ready != deviceInfo.isDeviceReady() || statusChange.connected != deviceInfo.isDeviceConnected()) {
                    if(statusChange.ready) {
                        textToShow = "microPay Ready";
                    }
                    else if(statusChange.connected) {
                        textToShow = "Device Connected"; }
                    else if(statusChange.paired) {
                        textToShow = "Device Disconnected";
                    }
                }
                deviceInfo.setDeviceConnected(statusChange.connected);
                deviceInfo.setDevicePaired(statusChange.paired);
                deviceInfo.setDeviceReady(statusChange.ready);
                deviceInfo.setSerialNumber(statusChange.pinpadName);

                if(!textToShow.equals("")) {
                    UiHelper.showToast(getActivity(), textToShow);
                }
            }
        });
//        authPosAndUser();
//        pairDevice();

    }

    private void authPosAndUser() {
        cloudEftposSDK.VerifyCredentials("test", "test", new OnAuthCompleted() {
            @Override
            public void OnAuthComplete(AuthCompleted authCompleted) {
                if (authCompleted.authorised) {
                    Application.info("User Verified");
                    cloudEftposSDK.AuthorisePOS("test", new OnAuthCompleted() {
                        @Override
                        public void OnAuthComplete(AuthCompleted authCompleted) {
                            if (authCompleted.authorised) {
                                Application.info("POS Verified");
                            } else {
                                Application.info("POS Verified failed" + authCompleted.error.toString());
                            }
                            UiHelper.showToast(getActivity(), authCompleted.authorised ? "POS Verified" : "Error verifying POS credentials");
                        }
                    });
                } else {
                    Application.info("User Verified failed" + authCompleted.error.toString());
                }
                UiHelper.showToast(getActivity(), authCompleted.authorised ? "UserVerified" : "Error verifying credentials");
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_test, container, false);
        editText = (EditText)rootView.findViewById(R.id.inputNumber);
        button = (Button)rootView.findViewById(R.id.sendButton);
        button.setOnClickListener(this);
        auth = (Button)rootView.findViewById(R.id.authPosButton);
        auth.setOnClickListener(this);
        back = (Button)rootView.findViewById(R.id.backButton);
        back.setOnClickListener(this);
        updateBackground(rootView);
        return rootView;
    }

    private int IncrementPosReference() {
       transactionReference++;
       SharedPreferences.Editor editor = userDetails.edit();
       editor.putInt("PosReference", transactionReference);
       editor.commit();
       return transactionReference;
    }

    private void PerformTransaction(CloudEftposSDK.TransactionType type, int amount, int cashOut, int tip) {

        final TransactionRequest request = new TransactionRequest();
        request.amount = amount;
        request.cashOut = cashOut;
        request.tipAmount = tip;
        request.transactionType = type;
        if (type == Refund) {
            request.verifyRefund = true;
        }
        request.posReference = String.valueOf(IncrementPosReference());

        Application.info("Amount: "+ request.amount + " CashOut: "+ request.cashOut+" Tip: "
                +request.tipAmount +" Transaction type: "+ request.transactionType.toString()
                + " Pos Reference: "+request.posReference.toString());

       /* boolean isTip = (request.tipAmount !=0);
        boolean isSurcharging = (request.cashOut !=0);
        HttpApi.getInstance().startPay(request.amount, isTip, isSurcharging,"demo/demo@tswwh.com", "11111111111",new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                int b;
            }

            @Override
            public void onResponse(Response response) throws IOException {
                ResponseBody body = response.body();

                try {
                    JSONObject json = new JSONObject(body.string());
                    int status = json.getInt("status");
                    if(status == 1)
                    {
                        JSONObject data = json.getJSONObject("data");
                        String id = data.getString("id");
                        Log.d(id,"");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });*/
        cloudEftposSDK.StartTransaction(false, request, new OnTransactionCompleted() {
                @Override
                public void OnComplete(TransactionCompleted transactionCompleted, ErrorResult errorResult) {
                    Application.info("PerformTransaction OnComplete");
                    if (errorResult != null) {
                        Application.info("ERROR_TRANSACTION" + errorResult.description + "\n" + " 'USER INFO': " + "\n" + userInfo.getString() + "\n" + " 'DEVICE INFO': " + "\n" + deviceInfo.getString());
                        UiHelper.showToast(getActivity(), errorResult.description);

                        //sendMessageIntent("'ERROR': " + errorResult.description + "\n" + " 'USER INFO': " + "\n" + userInfo.getString() + "\n" + " 'DEVICE INFO': " + "\n" + deviceInfo.getString());

                    } else {
                        Application.info("TRANSACTION_SUCCESS" + transactionCompleted.responseText + "\n" + " 'USER INFO': " + "\n" + userInfo.getString() + "\n" + " 'DEVICE INFO': " + "\n" + deviceInfo.getString() + "\n" + " 'TRANSACTION INFO': " + transactionCompleted.toString());
                        UiHelper.showToast(getActivity(), transactionCompleted.responseText);
                        boolean isTip = (transactionCompleted.tipAmount !=0);
                        boolean isSurcharging = (transactionCompleted.cashOut !=0);
                        HttpApi.getInstance().startPay(transactionCompleted.amount, isTip, isSurcharging,transactionCompleted.account, transactionCompleted.cardName,new Callback() {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                Log.d("FAILURE", "Fail");
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                ResponseBody body = response.body();
                                try {
                                    JSONObject json = new JSONObject(body.string());
                                    int status = json.getInt("status");
                                    if(status == 1)
                                    {
                                        JSONObject data = json.getJSONObject("data");
                                        String id = data.getString("id");
                                        Log.d(id,"");
                                        getmFragmentMangerHelper().loadFragmentByTag(CheckFragment.TAG, true,id);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }, getActivity());

        /*catch (Exception e)
        {
            Application.info("EXCEPTION: " + e.getStackTrace().toString());
        }*/
    }

    //Get last transaction
    private void getLastTransaction(CloudEftposSDK.TransactionType type, int amount,String posReference) {
        cloudEftposSDK.GetLastTransaction(type, amount, posReference, new OnTransactionCompleted() {
            @Override
            public void OnComplete(TransactionCompleted transactionCompleted, ErrorResult errorResult) {
                UiHelper.showToast(getActivity(), transactionCompleted.responseText);
            }
        });
    }

    //Recover last transaction
    private void recoverTransaction(CloudEftposSDK.TransactionType transactionType, int amount, String posReference)    {
        cloudEftposSDK.GetLastTransaction(transactionType,amount,posReference, new OnTransactionCompleted() {
            @Override
            public void OnComplete(TransactionCompleted transactionCompleted, ErrorResult errorResult) {
                UiHelper.showToast(getActivity(),transactionCompleted.responseText);
            }
        });
    }

    //Disconnect microPay
    private void disconnectDevice() {
        Application.info("disconnectDevice");
        cloudEftposSDK.DisconnectDevice();
    }

    //Check for updates
    private void updatePinPad() {
        cloudEftposSDK.UpdatePINpad(false, new OnUpdateCompleted() {
            @Override
            public void OnUpdateComplete(UpdateCompleted updateCompleted) {

            }
        });
    }

    private void pairDevice() {
        cloudEftposSDK.PairDevice(false, new OnPairingCompleted() {
            @Override
            public void OnComplete(PairingCompleted pairingCompleted) {
                if(pairingCompleted.errorCode != null && !pairingCompleted.errorCode.description.equals("")) {
                    Application.info("Error pairing"+ pairingCompleted.errorCode.description);
                    deviceInfo.setDevicePaired(false);
                    UiHelper.showToast(getActivity(),pairingCompleted.errorCode.description);
                }
                else {
                    Application.info("Pairing successfull"+ pairingCompleted.toString());
                    deviceInfo.setDevicePaired(true);
                    UiHelper.showToast(getActivity(),pairingCompleted.toString());
                }
            }
        });
    }

    // Verify Password
    private void verifyPassword() {
        cloudEftposSDK.VerifyPassword(userInfo.getPassword(),new OnAuthCompleted() {
            @Override
            public void OnAuthComplete(AuthCompleted authCompleted) {
                UiHelper.showToast(getActivity(),authCompleted.authorised ? "Password Authorized":"Invalid Password");
            }
        });
    }

    // Cancel Transaction
    private void cancelTransaction()
    {
        Application.info("cancelTransaction");
        cloudEftposSDK.CancelTransaction();
    }

    private void sendMessageIntent(String result) {
        Application.info("sendMessageIntent");
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"dmitry.cto@zuzex.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT,"Your payment");
        emailIntent.putExtra(Intent.EXTRA_TEXT,result);
        emailIntent.setType("message/rfc822");
        startActivity(Intent.createChooser(emailIntent,"Choose an email provider"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendButton: {
                if(!editText.getText().toString().equals("")) {
                    Application.info("press");
                    int amount = Integer.valueOf(editText.getText().toString()) *100;
                    PerformTransaction(Purchase, amount, 0, 0);
                }
                else {
                    UiHelper.showToast(getActivity(),"Please, enter the value");
                }
                break;
            }
            case R.id.backButton: {
                getmFragmentMangerHelper().loadFragmentByTag("MainFragment",true);
            }
            break;

            case R.id.authPosButton: {
                Application.info("auth");
                authPosAndUser();
                pairDevice();
            }
            break;
        }
    }
}
