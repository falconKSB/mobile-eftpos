package com.zuzex.eftpos.mobile;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;

import com.badoo.mobile.util.WeakHandler;
import com.zuzex.eftpos.mobile.Fragments.FragmentMangerHelper;
import com.zuzex.eftpos.mobile.Utils.LocationNotifier;

public class MainActivity extends Activity {
    private static final String TAG = "MainActivity";
    public boolean isVisible = false;

    public FragmentMangerHelper getmFragmentMangerHelper() {
        if(mFragmentMangerHelper != null) {
            return mFragmentMangerHelper;
        } else {
            return new FragmentMangerHelper(MainActivity.this, R.id.container, null);
        }
    }

    private FragmentMangerHelper mFragmentMangerHelper;
    private LocationNotifier mLocationNotifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //todo move to application child class
        mLocationNotifier = new LocationNotifier(this, new WeakHandler());
        mFragmentMangerHelper = new FragmentMangerHelper(MainActivity.this, R.id.container, savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onResume() {
        super.onResume();
        isVisible = true;
        checkIntent(getIntent());
    }


    @Override
    public void onPause() {
        super.onPause();
        isVisible = false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mFragmentMangerHelper.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            logout();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mFragmentMangerHelper.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mFragmentMangerHelper.onStop();
    }

    private void logout() {
        SharedPreferences userDetails = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor ed = userDetails.edit();
        ed.putString("userName", "");
        ed.putString("password", "");
        ed.putBoolean("isRemember", false);
        ed.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

        }
    }

    private void unknownResultCode(int result) {
//        UiHelper.showToast(this, "Unknown code" + String.valueOf(result));
    }

    private void checkIntent(Intent intent) {
//        try {
//            AnnounceModel announceModel = intent.getParcelableExtra(AnnounceModel.TAG);
//            boolean used = intent.getBooleanExtra(USED, false);
//            if (isVisible && !used) {
//                mLocationNotifier.alertDialogNotify(announceModel);
//            }
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
//        getIntent().removeExtra(AnnounceModel.TAG);
//        getIntent().setAction("");
//        getIntent().putExtra(USED, true);
    }

    public LocationNotifier getLocationNotifier() {
        return mLocationNotifier;
    }
}
