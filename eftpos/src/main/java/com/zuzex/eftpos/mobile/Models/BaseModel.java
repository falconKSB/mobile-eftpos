package com.zuzex.eftpos.mobile.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dgureev on 11/26/14.
 */

//{
//        "status": 1,
//        "message": "Login Success",
//        "data": {
//        "api_login": "demo/demo@tswwh.com",
//        "api_secret": "d355363c0f4a057bb4f405af2ca3fe47",
//        "logo": "http://web.dev.miretail.com.au/CContent/images/shared/avatar.png",
//        "logo_thumb": "http://web.dev.miretail.com.au/CContent/images/shared/avatar.png",
//        "background": "#000000"
//        }
//        }

public class BaseModel {
    public boolean isSuccessStatus;
    public String message;
    public String apiSecret;
    public String apiLogin;

    public JSONObject getData() {
        return data;
    }

    JSONObject data;

    public BaseModel(String object) throws JSONException {
        this(new JSONObject(object));
    }

    public BaseModel(JSONObject jsonObject) {
        this.message = jsonObject.optString("message");
        this.isSuccessStatus = (jsonObject.optInt("status") == 1);
        this.apiSecret = jsonObject.optString("api_secret");
        this.apiLogin = jsonObject.optString("api_login");
        this.data = jsonObject.optJSONObject("data");
    }
}
