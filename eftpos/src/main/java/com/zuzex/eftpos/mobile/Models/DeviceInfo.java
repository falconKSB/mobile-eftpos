package com.zuzex.eftpos.mobile.Models;

/**
 * Created by ksanchir on 12/11/14.
 */
public class DeviceInfo {

    private boolean devicePaired;
    private boolean deviceConnected;
    private boolean deviceReady;
    private String serialNumber;

    public DeviceInfo() {
        deviceConnected = false;
        devicePaired = false;
        deviceReady = false;
        serialNumber = "";
    }
    public DeviceInfo(boolean devicePaired, boolean deviceConnected, boolean deviceReady, String serialNumber) {
        this.deviceConnected = deviceConnected;
        this.devicePaired = devicePaired;
        this.deviceReady = deviceReady;
        this.serialNumber = serialNumber;
    }

    public boolean isDevicePaired()
    {
        return devicePaired;
    }
    public boolean isDeviceConnected()
    {
        return deviceConnected;
    }
    public boolean isDeviceReady()
    {
        return deviceReady;
    }
    public String getSerialNumber()
    {
        return serialNumber;
    }
    public void setDevicePaired(boolean devicePaired)
    {
        this.devicePaired = devicePaired;
    }
    public void setDeviceConnected(boolean deviceConnected) {
        this.deviceConnected = deviceConnected;
    }
    public void setDeviceReady(boolean deviceReady)
    {
        this.deviceReady = deviceReady;
    }
    public void setSerialNumber(String serialNumber)
    {
        this.serialNumber = serialNumber;
    }
    public String getString() {
        return "'SERIAL NUMBER': " + serialNumber + " 'IS DEVICE CONNECTED': " +deviceConnected + " 'IS DEVICE PAIRED': " +devicePaired + " 'IS DEVICE READY': "+deviceReady;
    }
}
