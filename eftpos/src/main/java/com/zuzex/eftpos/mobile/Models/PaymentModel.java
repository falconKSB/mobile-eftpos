package com.zuzex.eftpos.mobile.Models;

/**
 * Created by ksanchir on 12/11/14.
 */
public class PaymentModel {

    private String userID;
    private double amount;
    private boolean isTip;
    private boolean isSurcharging;
    private String account;
    private String creditNum;

    public PaymentModel() {
        userID = "";
        amount = 0.0;
        isTip = false;
        isSurcharging = false;
        account = "";
        creditNum = "";
    }
    public PaymentModel(String userID, double amount, boolean isTip, boolean isSurcharging, String account, String creditNum) {
        this.userID = userID;
        this.amount = amount;
        this.isTip = isTip;
        this.isSurcharging = isSurcharging;
        this.account = account;
        this.creditNum = creditNum;
    }
    public String UserID()
    {
        return userID;
    }
    public double Amount()
    {
        return amount;
    }
    public boolean IsTip()
    {
        return isTip;
    }
    public boolean IsSurcharging()
    {
        return isSurcharging;
    }
    public String Account()
    {
        return account;
    }
    public String CreditNum()
    {
        return creditNum;
    }

    public void setUserID(String userID)
    {
        this.userID = userID;
    }
    public void setAmount(double amount)
    {
        this.amount = amount;
    }
    public void setIsTip(boolean isTip)
    {
        this.isTip = isTip;
    }
    public void setIsSurcharging(boolean isSurcharging)
    {
        this.isSurcharging = isSurcharging;
    }
    public void setAccount(String account)
    {
        this.account = account;
    }
    public void setCreditNum(String creditNum)
    {
        this.creditNum = creditNum;
    }

}
