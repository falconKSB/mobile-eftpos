package com.zuzex.eftpos.mobile.Models;

/**
 * Created by ksanchir on 12/11/14.
 */
public class UserInfo {
    private String emailAddress;
    private String password;

    public UserInfo() {
        emailAddress = "";
        password = "";
    }
    public UserInfo(String emailAddress, String password) {
        this.emailAddress = emailAddress;
        this.password = password;
    }
    public String  getEmailAddress()
    {
        return emailAddress;
    }
    public String getPassword()
    {
        return password;
    }
    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }
    public String getString()
    {
        return "'EMAIL': " +emailAddress+" 'PASSWORD': " + password;
    }
}
