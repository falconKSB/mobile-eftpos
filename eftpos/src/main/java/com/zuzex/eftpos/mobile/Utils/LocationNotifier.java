package com.zuzex.eftpos.mobile.Utils;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.text.Html;
import android.widget.RemoteViews;

import com.badoo.mobile.util.WeakHandler;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.zuzex.eftpos.mobile.api.HttpApi;

import org.json.JSONException;

import java.io.IOException;

/**
 * Created by dgureev on 11/27/14.
 */
public class LocationNotifier {
    public String mTitle = "" ;
    private Context mContext;
    WeakHandler mHandler;
    AlertDialog alertDialog;
    NotificationManager mNotificationManager;

    public LocationNotifier(Context context, WeakHandler handler) {
        this.mContext = context;
        this.mHandler = handler;
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mTitle = mContext.getApplicationContext().getPackageName();
    }



//    public void showPushNotification(final AnnounceModel model) {
//        if(model != null) {
//            Intent targetIntent = new Intent(mContext, MainActivity.class);
//            targetIntent.putExtra(AnnounceModel.TAG, model);
//            int width = mContext.getResources().getDimensionPixelSize(android.R.dimen.notification_large_icon_width);
//            int height = mContext.getResources().getDimensionPixelSize(android.R.dimen.notification_large_icon_height);
//            Bitmap largeIcon = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_launcher), width, height, true);
//            PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, targetIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            long[] pattern = {100, 300, 100, 300};
//
//            RemoteViews contentView = new RemoteViews(mContext.getPackageName(), R.layout.device_notification);
//            contentView.setImageViewResource(R.id.image, R.drawable.ic_launcher);
//            contentView.setTextViewText(R.id.title, mTitle);
//            contentView.setTextViewText(R.id.text, Html.fromHtml(model.message));
//
//            NotificationCompat.Builder mBuilder =
//                    new NotificationCompat.Builder(mContext)
//                            .setLargeIcon(largeIcon)
//                            .setSmallIcon(R.drawable.ic_launcher)
//                            .setSound(alarmSound)
//                            .setVibrate(pattern)
//                            .setLights(Color.GREEN, 1000, 3000)
//                            .setContent(contentView)
//                            .setAutoCancel(true);
//            mBuilder.setContentIntent(contentIntent);
//            mNotificationManager.notify(0, mBuilder.build());
//        }
//    }

//    public void alertDialogNotify(final AnnounceModel announceModel) {
//        if(announceModel == null || announceModel.message == null) {
//            return;
//        }
//        if (alertDialog != null) {
//            alertDialog.dismiss();
//        }
//        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
//        builder.setTitle(mContext.getString(R.string.location_dialog_title));
//        builder.setMessage(Html.fromHtml(announceModel.message));
//        if (announceModel.isValudUrl()) {
//            builder.setMessage(announceModel.url);
//            builder.setPositiveButton(mContext.getString(R.string.open_in_browser), new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(announceModel.url));
//                    mContext.startActivity(browserIntent);
//                }
//            });
//        } else {
//            builder.setPositiveButton(mContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    alertDialog.dismiss();
//                    alertDialog = null;
//                }
//            });
//        }
//        alertDialog = builder.create();
//        alertDialog.show();
//    }
}
