package com.zuzex.eftpos.mobile.activitySDK;
import com.questps.cloudeftpossdk.CloudEftposSDK;
import com.questps.cloudeftpossdk.messages.TransactionRequest;
import com.zuzex.eftpos.mobile.Models.DeviceInfo;
import com.zuzex.eftpos.mobile.Models.UserInfo;

import static com.questps.cloudeftpossdk.CloudEftposSDK.TransactionType.Refund;
/**
 * Created by ksanchir on 12/11/14.
 */
public class ActivitySDK {
    private static CloudEftposSDK cloudEftposSDK;
    private UserInfo userInfo;
    private DeviceInfo deviceInfo;

    public ActivitySDK()
    {

    }
    private void PerformTransaction(CloudEftposSDK.TransactionType type, int amount, int cashOut, int tip)
    {
        TransactionRequest request = new TransactionRequest();
        request.amount = amount;
        request.cashOut = cashOut;
        request.tipAmount = tip;
        request.transactionType = type;
        if (type == Refund)
        {
            request.verifyRefund = true;
        }
        //request.posReference = String.valueOf(IncrementPosReference());
       /* cloudEftposSDK.StartTransaction(false,request,new OnTransactionCompleted() {
            @Override
            public void OnComplete(TransactionCompleted transactionCompleted, ErrorResult errorResult) {

            }
        });*/
    }

}
