package com.zuzex.eftpos.mobile.api;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.zuzex.eftpos.mobile.Models.PaymentModel;

import org.apache.http.auth.UsernamePasswordCredentials;

import javax.inject.Inject;

/**
 * Created by dgureev on 11/19/14.
 */
public class HttpApi {
    @Inject
    static DigestAuthenticator mDigestAuthenticator;
    private static String baseHost = "http://api.v1.dev.miretail.com.au";
    private static final int port = 80;

    private static volatile HttpApi instance;

    public boolean isAuthorized = false;

    private static OkHttpClient client = new OkHttpClient();

    private static OkHttpClient createAsyncClient() {
        OkHttpClient okHttpClient = new OkHttpClient();
        return okHttpClient;
    }

    public static HttpApi getInstance() {
        HttpApi localInstance = instance;
        if (localInstance == null) {
            synchronized (HttpApi.class) {
                client = createAsyncClient();
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new HttpApi();
                }
            }
        }
        return localInstance;
    }

    public static String getBaseHost() {
        return baseHost;
    }

    public static void post(String url, RequestBody body, Callback callback) {
        Request request = new Request.Builder()
                .url(baseHost+url)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    public static void get(String url, Callback callback) {
        Request request = new Request.Builder()
                .url(baseHost+url)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    public void login(final String userName, final String password, Callback callback) {
        mDigestAuthenticator = new DigestAuthenticator(new UsernamePasswordCredentials(userName, password));
        client.setAuthenticator(mDigestAuthenticator);
        get("/login", callback);
    }

    public void registerDevice(final String deviceId, final String deviceType, final String name, Callback callback) {
        String url = "/device/register";
        RequestBody request = new FormEncodingBuilder()
                .add("id", deviceId)
                .add("type", deviceType)
                .add("name", name)
                .build();
        post(url, request, callback);
    }
    public void startPay(int amount,boolean isTip, boolean isSurcharging,String account,String creditNum, Callback callback) {
        String url = "/pospay/start";
        RequestBody request = new FormEncodingBuilder()
                .add("amount", Integer.toString(amount))
                .add("tip", Boolean.toString(isTip))
                .add("surcharge", Boolean.toString(isSurcharging))
                .add("account",account)
                .add("creditcard",creditNum)
                .build();
        post(url, request, callback);
    }


    public void startPayment(PaymentModel paymentModel, Callback callback) {

    }
}